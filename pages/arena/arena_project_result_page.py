from selenium.webdriver.common.by import By


class ProjectResultPage:
    def __init__(self, browser):
        self.browser = browser

    def go_to_projects(self):
        self.browser.find_element(By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/projects']").click()

    def verify_created_project(self, name_of_the_project):
        assert name_of_the_project in self.browser.find_element(By.CSS_SELECTOR, 'Kiciakocia').text

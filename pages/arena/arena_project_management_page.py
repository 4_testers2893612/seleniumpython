from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from utils.random_message import generate_random_text


class ArenaProjectManagementPage:
    def __init__(self, browser):
        self.browser = browser

    def add_new_project(self):
        self.browser.find_element(By.CSS_SELECTOR,
                                  "a[href='http://demo.testarena.pl/administration/add_project']").click()

    def add_info_to_project(self, name_of_the_project):
        self.browser.find_element(By.CSS_SELECTOR, "#name").send_keys(name_of_the_project)
        self.browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys(generate_random_text(10))
        self.browser.find_element(By.CSS_SELECTOR, "#description").send_keys(generate_random_text(25))

    def save_project(self):
        self.browser.find_element(By.CSS_SELECTOR, "input#save").click()

    def search_for_project(self, name_of_the_project):
        self.browser.find_element(By.CSS_SELECTOR, "#search").send_keys(name_of_the_project + Keys.ENTER)

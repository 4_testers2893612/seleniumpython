from selenium.webdriver.common.by import By


class HomePage:
    # Inicjalizacja klasy - przekazanie drivera (przegladarki)
    def __init__(self, browser):
        self.browser = browser

    def verify_post_count(self, expected_count):
        list_titles = self.browser.find_elements(By.CSS_SELECTOR, '.post-title')
        assert len(list_titles) == expected_count

    def search_for(self, query):
        search_input = self.browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
        search_button = self.browser.find_element(By.CSS_SELECTOR, 'input.gsc-search-button')
        search_input.send_keys(query)
        search_button.click()

    def click_label(self, label_text):
        label_element = self.browser.find_element(By.LINK_TEXT, label_text)
        label_element.click()

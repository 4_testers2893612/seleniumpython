import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC, expected_conditions

from pages.home import HomePage
from pages.search_result import SearchResultPage


@pytest.fixture()
def browser():
    # 1 to wykona się PRZED każdym testem który korzysta z tego fixtur'a
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('https://awesome-testing.blogspot.com/')

    # 2 To jest (zmienna) to co zwracamy (przekazujemy) do testów
    yield driver
    # 3 to wykona się PO każdym teście który korzysta z tego fixtur'a
    driver.quit()

def test_post_count(browser):
    home_page = HomePage(browser)
    home_page.verify_post_count(4)

    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 4 elementy
    assert len(titles) == 4


def test_post_count_after_search(browser):
    home_page = HomePage(browser)
    home_page.search_for('selenium')

    search_result_page = SearchResultPage(browser)
    search_result_page.wait_for_load()
    search_result_page.verify_post_count(20)


def test_post_count_on_2016_label(browser):
    # Inicjalizacja elementu z labelką 2016
    label_2016 = browser.find_element(By.LINK_TEXT, '2016')

    # Kliknięcie na labelkę
    label_2016.click()

    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 24 element
    assert len(titles) == 24


import time

from selenium.webdriver import Chrome, Keys
from selenium.webdriver.common import keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


# Test - uruchomienie Chroma
def test_searching_in_google():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://google.pl')
    browser.set_window_size(1920, 1080)
    time.sleep(3)

    # Akceptacja ciastka
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.ID, 'L2AGLb')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))
    browser.find_element(By.ID, 'L2AGLb').click()

    # Znalezienie paska wyszukiwania
    search_input = browser.find_element(By.CSS_SELECTOR, '#APjFqb')

    # Asercja
    assert search_input.is_displayed() is True

    # Szukanie 4testers
    search_input.send_keys('4testers' + Keys.ENTER)

    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()

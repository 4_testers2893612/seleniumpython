import time

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


# Test - uruchomienie Chroma
def test_should_login_successfully():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://demo.testarena.pl/zaloguj')

    # Zalogować się na testarenie
    browser.find_element(By.ID, 'email').send_keys('administrator@testarena.pl')
    browser.find_element(By.ID, 'password').send_keys('sumXQQ72$L')
    browser.find_element(By.ID, 'login').click()

    # Asercja - weryfikacja że logowanie faktycznie się udało
    user_info = browser.find_element(By.CSS_SELECTOR, '.user-info small')
    assert user_info.text == 'administrator@testarena.pl'

    # sprawiamy żeby przeglądarka była na całym oknie
    browser.set_window_size(1920, 1080)
    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'TestArena' in browser.title

    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()
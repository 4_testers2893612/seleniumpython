import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_messages import ArenaMessagesPage
from pages.arena.arena_project_page import ArenaProjectPage


@pytest.fixture
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()


def test_should_display_email_in_user_section(browser):
    user_info = browser.find_element(By.CSS_SELECTOR, '.user-info small')
    assert user_info.text == 'administrator@testarena.pl'


def test_should_successfully_logout(browser):
    browser.find_element(By.CSS_SELECTOR, '.icons-switch').click()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'


def test_should_open_messages_and_display_text_area(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_mail').click()
    wait = WebDriverWait(browser, 10)
    wait.until(expected_conditions.element_to_be_clickable((By.CSS_SELECTOR, '#j_msgContent')))


def test_should_open_project_page(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

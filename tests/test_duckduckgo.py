import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://duckduckgo.com/')

    #Ustawienie rodzielczości Full HD
    browser.set_window_size(1920, 1080)

    # Znalezienie paska wyszukiwania
    search_input = browser.find_element(By.CSS_SELECTOR, '#search_form_input_homepage')

    # Znalezienie guzika wyszukiwania (lupki)
    search_button = browser.find_element(By.CSS_SELECTOR, '#search_button_homepage')

    # Asercje że elementy są widoczne dla użytkownika
    assert search_input.is_displayed()
    assert search_button.is_displayed()

    # Szukanie 4testers
    search_input.send_keys('4tester')
    search_button.click()

    # Sprawdzenie że jakikolwiek wynik ma tytuł '4testers'
    # lista Seleniumowych WebElementów, które reprezentują nam tytuły stron
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, 'h2 a')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))
    results = browser.find_elements(By.CSS_SELECTOR, 'h2 a')